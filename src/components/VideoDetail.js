import React from 'react'

const VideoDetail = ({video}) => {

    const renderContent = () => {
        if(video == null) {
            return null;
        }

        return (
            <div className="ui segment">
                <img className="ui fluid image" alt={video.title} src={video.img} />
            </div>
        );
    }


    return (
        <div>
            {renderContent()}
        </div>
    )
}

export default VideoDetail
