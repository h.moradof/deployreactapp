import React, { useState, useEffect } from 'react'
import SearchBar from './SearchBar'
import VideoList from './VideoList'
import VideoDetail from './VideoDetail';
import useVideos from './hooks/useVideos'
import Header from './Header'

function App() {

  const [selectedVideo, setSelectedVideo] = useState(null);
  const [videos, search] = useVideos('something');

  useEffect(() => {
    setSelectedVideo(videos[0]);
  }, [videos]);

  return (
    <div className="ui container">
      <Header />
      <br />
      <SearchBar onSearch={search} />
      <br />
      <VideoDetail video={selectedVideo} />
      <br/>
      <VideoList videos={videos} onSelect={setSelectedVideo} />
    </div>
  );
}

export default App
