import React from 'react'

const VideoItem = ({ video, onSelect }) => {

    const onSelectVideo = () => {
        onSelect(video);
    }

    return (
        <div className="column">
            <div className="ui fluid card" onClick={onSelectVideo} >
                <div className="image">
                    <img alt={video.title} src={video.img} />
                </div>
                <div className="content">
                    <h3 className="header">{video.title}</h3>
                </div>
            </div>
        </div>
    )
}

export default VideoItem
