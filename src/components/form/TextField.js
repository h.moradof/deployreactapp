import React from 'react'

const TextField = ({value, onChange, placeholder, labelText }) => {
    return (
        <div className="field">
            {
                labelText && labelText !== '' ?
                    <label className="label">{labelText}</label>
                        : null
            }
            <input
                className="input"
                type="text"
                value={value}
                onChange={e => onChange(e.target.value)}
                placeholder={placeholder}
            />
        </div>
    )
}

export default TextField;