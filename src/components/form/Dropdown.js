import React, { useState, useEffect, useRef } from 'react'

const Dropdown = ({ labelText, options, selected, onSelectedChange }) => {
    const [open, setOpen] = useState(false);
    const ref = useRef();

    const onBodyClick = (event) => {
        if(!ref.current.contains(event.target)) { 
            setOpen(false);
        }
    }

    useEffect(() => {
        document.body.addEventListener('click', onBodyClick);

        return () =>{
           document.body.removeEventListener('click', onBodyClick);
        };
    }, [])

    const onChangeSelectedOption = (option) => {
        onSelectedChange(option);
        //setOpen(false); // we do not need this because of "event bubbling"
    }

    return (
        <div ref={ref} className="ui form">
            <div className="field">
                <label className="label">{labelText}</label>
                <div className={`ui selection dropdown ${open ? 'active visible' : ''}`} onClick={() => setOpen(!open)}>
                    <i className="dropdown icon"></i>
                    <div className="text">{selected.label}</div>
                    <div className={`menu ${open ? 'transition visible' : ''}`}>
                        {
                            options
                            .filter(o => o.value !== selected.value)
                            .map(o => {
                                return <div onClick={() => onChangeSelectedOption(o)} key={o.value} className="item" data-value={o.value}>{o.label}</div>
                            })
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Dropdown;
