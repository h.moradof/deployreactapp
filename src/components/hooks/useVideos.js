import { useState, useEffect } from 'react'
import youtube from '../api/YouTube'

const useVideos = (defaultSearchTerm) => {
    const [videos, setVideos] = useState([]);

    useEffect(() => {
        search(defaultSearchTerm);
    }, [defaultSearchTerm]);


    const search = (term) => {
        var result = youtube.search(term);
        setVideos(result);
    }

    return [videos, search ];
}

export default useVideos