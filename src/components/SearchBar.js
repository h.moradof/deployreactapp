import React, { useState, useEffect } from 'react'
import TextField from './form/TextField'

const SearchBar = ({ onSearch }) => {
    
    const [term, setTerm] = useState('');
    const [bounceTerm, setBounceTerm] = useState(term);
    
    useEffect(() => {
        const timerId = setTimeout(() => {
            setBounceTerm(term);
        }, 1000);

        return () => {
            clearTimeout(timerId);
        }
    }, [term])

    useEffect(() => {        
        if(bounceTerm.length > 0) {
            onSearch(bounceTerm); 
        }       
    }, [bounceTerm , onSearch])

    return (
        <div className="ui form">
            <TextField value={term} onChange={setTerm} placeholder="search video" />
        </div>
    )
}

export default SearchBar
