import React from 'react'
import VideoItem from './VideoItem'

const VideoList = ({ videos, onSelect }) => {

    const renderContent = () => {
        if(videos.lenght === 0) {
            return 'nothimg found';
        }

        return videos.map(v => {
            return <VideoItem key={v.id} video={v} onSelect={onSelect} />
        });
    }    

    return (
        <div className="ui four column grid">
            {renderContent()}
        </div>
    )
}

export default VideoList
